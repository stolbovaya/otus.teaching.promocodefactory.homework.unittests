﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;


        public SetPartnerPromoCodeLimitRequest CreatePartnerPromoCodeLimitRequest()
        {
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = new DateTime(2024, 10, 9),
                Limit = 100

            };

            return request;
        }
        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }
        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }


        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFind_ReturnsBadRequest()
        {
            // Arrange

            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;
            SetPartnerPromoCodeLimitRequest request = null;


            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            partner.IsActive = false;
            SetPartnerPromoCodeLimitRequest request = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }




        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerSetLimit_NumberIssuedPromoCodesIs0()
        {
            // Arrange
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            var partner = CreateBasePartner();
            var request = CreatePartnerPromoCodeLimitRequest();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert

            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerSetLimit_CancelDateSetNow()
        {
            // Arrange
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            var partner = CreateBasePartner();
            var request = CreatePartnerPromoCodeLimitRequest();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
                x.CancelDate.HasValue);
            // Assert
            activeLimit.Should().NotBeNull();
        }



        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitLess1_ReturnsBadRequest()
        {
            // Arrange

            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            var partner = CreateBasePartner();
            var request = CreatePartnerPromoCodeLimitRequest();
            request.Limit = 0;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitMore1()
        {
            // Arrange

            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            var partner = CreateBasePartner();
            var request = CreatePartnerPromoCodeLimitRequest();
            request.Limit = 10;
            int countLimitFirst = partner.PartnerLimits.Count;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);


            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            //_partnersRepositoryMock.Setup(repo => repo.UpdateAsync(partner));
            //    .ReturnsAsync(partner);

            // Act

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            var resultLimit = (countLimitFirst + 1 == partner.PartnerLimits.Count);


            // Assert

            resultLimit.Should().BeTrue();
        }

        public override bool Equals(object obj)
        {
            return obj is SetPartnerPromoCodeLimitAsyncTests tests &&
                   EqualityComparer<Mock<IRepository<Partner>>>.Default.Equals(_partnersRepositoryMock, tests._partnersRepositoryMock) &&
                   EqualityComparer<PartnersController>.Default.Equals(_partnersController, tests._partnersController);
        }



        //TODO: Add Unit Tests
    }
}