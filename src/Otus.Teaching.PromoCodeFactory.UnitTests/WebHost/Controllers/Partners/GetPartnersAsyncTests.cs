﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using Xunit;
using FluentAssertions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using YamlDotNet.Core;
using System.Linq;
using Microsoft.AspNetCore.Localization;
using System.Collections;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class GetPartnersAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        
        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }
        public GetPartnersAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }




        [Fact]
        public async void GetPartnersAsync_ResultIsValid()
        {
            // Arrange
            var partner = CreateBasePartner();
            var partnerList = new List<Partner>();
            partnerList.Add(partner);
            var response = partnerList.Select(x => new PartnerResponse()
            {
                Id = x.Id,
                Name = x.Name,
                NumberIssuedPromoCodes = x.NumberIssuedPromoCodes,
                IsActive = x.IsActive,
                PartnerLimits = x.PartnerLimits
                    .Select(y => new PartnerPromoCodeLimitResponse()
                    {
                        Id = y.Id,
                        PartnerId = y.PartnerId,
                        Limit = y.Limit,
                        CreateDate = y.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        EndDate = y.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        CancelDate = y.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss"),
                    }).ToList()
            });


            _partnersRepositoryMock.Setup(repo => repo.GetAllAsync())
                 .ReturnsAsync(partnerList);

            // Act

            //var response = new PartnerResponse();

            var responseResult = await _partnersController.GetPartnersAsync();


            var ok = responseResult.Result as OkObjectResult;

            // Assert
            var list = (ok.Value as IEnumerable<PartnerResponse>).ToList();


            // Assert

            ok.Should().BeAssignableTo<OkObjectResult>();
            response.Should().BeEquivalentTo(list);
        }

      
        //TODO: Add Unit Tests
    }
}